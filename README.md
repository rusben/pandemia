# Pandemia - COVID-19 update

## Key Features

* Win other players with no effort.
  - Instantly see how the table is changing.
* Enemies AI improved.
* A huge community behind the back.

## How to compile?
```bash
# Go into the project source folder (src)
$ cd pandemia/src

# Compile the Main class
$ javac net/xeill/elpuig/Main.java

# Run the Main
$ java net.xeill.elpuig.Main
```
