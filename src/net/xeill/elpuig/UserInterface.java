package net.xeill.elpuig;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit ;

/**
* This class implements the Blackjack UI, User Interface.
*
* @author  ASIX1
*
*/

public class UserInterface {

  Scanner scanner = new Scanner(System.in);

  public String menuJugadorMover() {
        // L R U D
        return "L";
  }

  public String menuConfiguraJugador() {
    System.out.println("Introduce el nombre del jugador:");
    return scanner.nextLine();
  }

  public int menuEligeDificultadMundo() {

    System.out.println("Elige la dificultad del juego:");

    System.out.println("1 - FÁCIL");
    System.out.println("2 - MEDIO");
    System.out.println("3 - DIFÍCIL");

    String s = scanner.nextLine();
    int option = Integer.parseInt(s);

    switch (option) {
      case 1: return menuIniciaFacil();
      case 2: return menuIniciaMedio();
      case 3: return menuIniciaDificil();
      default:
        // Si no elige ninguna correcta, volvemos a llamarnos
        return menuEligeDificultadMundo();
    }

  }

  public int menuIniciaFacil() {
    return 1;
  }


  public int menuIniciaMedio() {
    return 2;
  }

  public int menuIniciaDificil() {
    return 3;
  }


  /**
  * Ask a name
  */
  String askName() {
    String name = scanner.nextLine();
    return name;
  }

  /**
  * Clears the screen.
  */
  void clearScreen() {
    System.out.print("\033\143");
  }

  /**
  * Shows the "YOU LOSE" message
  */
  void showYouLose() {
    System.out.println("\n\033[30;101m" +
                "  ╦ ╦╔═╗╦ ╦  ╦  ╔═╗╔═╗╔═╗  \n" +
                "  ╚╦╝║ ║║ ║  ║  ║ ║╚═╗║╣   \n" +
                "   ╩ ╚═╝╚═╝  ╩═╝╚═╝╚═╝╚═╝  \033[0m");
  }

  /**
  * Shows the "YOU WIN" message
  */
  void showYouWin(){
    System.out.println("\n\033[30;102m" +
              "  ╦ ╦╔═╗╦ ╦  ╦ ╦ ╦ ╔╗╔  \n" +
              "  ╚╦╝║ ║║ ║  ║║║ ║ ║║║  \n" +
              "   ╩ ╚═╝╚═╝  ╚╩╝ ╩ ╝╚╝  \033[0m");
  }

  /**
  * Shows the game status
  */
  void showStatus() {
  }

  void pressAnyKey(){
    scanner.nextLine();
  }

  /**
  * Pauses the game interaction during seconds
  */
  public void sleep(int seconds) {

    try {
      TimeUnit.SECONDS.sleep(seconds);
    } catch (Exception e) {

    }
  }


}
