package net.xeill.elpuig;

import java.util.*;
import java.io.*;

/**
* This class implements the Main.
*
* @author  ASIX1
*/
public class Jugador {

  int lives;
  String name;
  int ammunition;
  int x;
  int y;

  Jugador(int lives, String name, int ammunition) {
    this.lives = lives;
    this.name = name;
    this.ammunition = ammunition;
    int x = -1; // Significa que aún no ha sido posicionado
    int y = -1; // Significa que aún no ha sido posicionado
  }

  public void move(int x, int y) {
    this.x = x;
    this.y = y;
  }

}
