package net.xeill.elpuig;

import java.util.*;
import java.io.*;

/**
* This class implements the Main.
*
* @author  ASIX1
*/
public class Pandemia {

  char[][] world;
  int viruses;
  int population;
  char v; // virus character
  char p; // population character


  Pandemia(int level, char v, char p, Jugador player) {
    if (level == 1) {
      world = new char[10][10];
      viruses = 10;
      population = 30;
      this.v = v;
      this.p = p;

      iniciaFacil();
    }
  }

  public void iniciaFacil() {
    // Distribuir población viruses por la matriz
    // y también al jugador
  }

  public void moverVirus() {

  }

  public void moverPersonas() {

  }

  public void moverJugador() {

  }

  public void print() {
    // Print the world state
  }

  public void movimientos() {
    // Mueve jugador, personas y virus

    moverJugador();
    moverPersonas();
    moverVirus();


  }

  public boolean defeated() {
    // Indica si el virus ha sido derrotado.
    return false;
  }
}
