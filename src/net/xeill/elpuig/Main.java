package net.xeill.elpuig;

import java.util.*;
import java.io.*;

/**
* This class implements the Main.
*
* @author  ASIX1
*/
public class Main {
  public static void main(String[] args) {

    UserInterface ui = new UserInterface();
    // Player 3 vidas, 20 balas
      Jugador player = new Jugador(3, ui.menuConfiguraJugador(), 20);
      int nivel = ui.menuEligeDificultadMundo();

      Pandemia covid = new Pandemia(nivel, '#', '@', player);

      while (player.lives != 0) {

        covid.movimientos();
        covid.print();

        if (covid.defeated()) {
          break;
        }

      }

      if (player.lives > 0) ui.showYouWin();
      else ui.showYouLose();


  }
}
